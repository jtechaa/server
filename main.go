package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	_ "github.com/lib/pq"
	"gitlab.com/jtechaa/server/db"
	"gitlab.com/jtechaa/server/helpers"
)

var repo *db.PostgreSQLRepository

func main() {
	if os.Getenv("PORT") == "" {
		log.Fatalln("PORT env var unset")
	}
	if os.Getenv("DATABASE_URL") == "" && os.Getenv("DB_HOST") == "" {
		log.Fatalln("Set either DATABASE_URL or DB_{HOST,PORT,USER,PW}")
	}

	log.Println("Initializing repository...")
	repo = initRepo()
	log.Println("Done.")

	http.HandleFunc("/addCourse", httpAddCourse)
	http.HandleFunc("/course", httpGetCourse)
	http.HandleFunc("/addProfessor", httpAddProfessor)
	http.HandleFunc("/professor", httpGetProfessor)
	http.HandleFunc("/addTutor", httpAddTutor)
	http.HandleFunc("/tutor", httpGetTutor)

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

// initRepo creates the sqlite database file and all the necessary tables within that file.
func initRepo() *db.PostgreSQLRepository {
	var conn string
	var dbname string

	if _, isDebug := os.LookupEnv("DEBUG"); isDebug {
		dbname = "jtechdev"
	} else {
		dbname = "jtech"
	}

	if os.Getenv("DATABASE_URL") != "" {
		log.Println("Ignoring all vars and connecting with DATABASE_URL...")
		conn = os.Getenv("DATABASE_URL")
	} else {
		log.Printf("Connecting with user " + os.Getenv("DB_USER") + ", host & port " + os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT") + ", db name " + dbname + "...\n")
		conn = "postgres://" + os.Getenv("DB_USER") + ":" + os.Getenv("DB_PW") + "@" + os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT") + "/" + dbname + "?sslrootcert=global-bundle.pem&sslmode=verify-full"
	}

	dbFile, err := sql.Open("postgres", conn)
	if err != nil {
		log.Fatal(err)
	}

	repo := db.NewPostgreSQLRepo(dbFile)
	if err != nil {
		log.Fatal(err)
	}

	err = repo.Migrate()
	if err != nil {
		log.Fatal(err)
	}

	return repo
}

// enableCors writes the CORS header to the HTTP response. If we are running locally, the value will be '*'; otherwise it will be the production website.
func enableCors(w http.ResponseWriter) {
	if _, isDebug := os.LookupEnv("DEBUG"); isDebug {
		w.Header().Set("Access-Control-Allow-Origin", "*")
	} else {
		w.Header().Set("Access-Control-Allow-Origin", os.Getenv("CORS_DOMAIN"))
	}
}

// httpAddCourse accepts a POST form request containing the course prefix, number, and suffix & creates a new entry in the courses table.
func httpAddCourse(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Cannot read body", http.StatusBadRequest)
			return
		}

		course, err := db.InitCourse(r.PostForm.Get("coursePrefix"), r.PostForm.Get("courseNumber"), r.PostForm.Get("courseSuffix"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		_, err = repo.InsertCourse(course)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Successfully added course, or course already exists.")
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "addCourse only accepts a POST form request.")
	}
}

// httpGetCourse retrieves a course from the database based on its full name. Partial matches are not considered.
func httpGetCourse(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodGet {
		query := r.URL.Query()

		c, err := repo.GetCourseByName(query.Get("fullName"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		cJ, err := json.Marshal(*c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(cJ)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "course only accets a GET request.")
	}
}

// httpAddProfessor accepts a POST form request containing the Professor's first and last name, one full course name, one secular & one Jewish-related rating.
// If the Course referenced by the course name does not exist, it is inserted into the database. If the Professor does not already exist, they are inserted into the database.
// If the Professor already exists, and they do not currently have the given course listed for them, their entry is updated to include the new course.
func httpAddProfessor(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Cannot read body", http.StatusBadRequest)
			return
		}

		prof := db.InitProfessor(r.Form.Get("fname"), r.Form.Get("lname"))

		// Read/insert course
		var profCourse *db.Course
		courseFullName := r.Form.Get("courseName")
		profCourse, err = repo.GetCourseByName(r.Form.Get("courseName"))
		if err != nil {
			if err.Error() == "course name too short" {
				http.Error(w, "Course name too short", http.StatusBadRequest)
				return
			}

			// Init new course since our error is likely that it doesn't exist

			var postfix string
			if len(courseFullName) == 8 {
				postfix = courseFullName[7:8]
			} else {
				postfix = ""
			}

			tmpCourse, err := db.InitCourse(courseFullName[:3], courseFullName[3:7], postfix)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			profCourse, err = repo.InsertCourse(tmpCourse)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}

		prof.Courses = append(prof.Courses, *profCourse)

		// Ratings
		prof.JewishRatings[courseFullName] = append(prof.JewishRatings[courseFullName], helpers.CapString(r.Form.Get("jewishrating"), 3000))
		prof.SecularRatings[courseFullName] = append(prof.SecularRatings[courseFullName], helpers.CapString(r.Form.Get("secularrating"), 3000))

		_, err = repo.InsertProfessor(prof)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Successfully added professor")
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "addProfessor only accepts a POST form request.")
	}
}

// httpGetProfessor retrieves a professor from the database based on their first and last name. Partial matches are not considered.
func httpGetProfessor(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodGet {
		query := r.URL.Query()

		p, err := repo.GetProfessorByName(query.Get("fname"), query.Get("lname"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		pJ, err := json.Marshal(*p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(pJ)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "course only accets a GET request.")
	}
}

func httpAddTutor(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, "Cannot read body", http.StatusBadRequest)
			return
		}

		tutor := db.InitTutor(r.Form.Get("fname"), r.Form.Get("lname"), r.Form.Get("affiliation"))
		courseList := helpers.CapString(r.Form.Get("courseList"), 350)
		if len(courseList) < 8 {
			http.Error(w, "you must specify at least one course", http.StatusBadRequest)
			return
		}

		for _, course := range strings.Split(courseList, ",") {
			c, err := repo.GetCourseByName(course)
			if err != nil {
				http.Error(w, "course "+course+" not found in system", http.StatusInternalServerError)
				return
			}
			tutor.Courses = append(tutor.Courses, *c)
		}

		_, err = repo.InsertTutor(tutor)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Successfully added tutor")
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "addTutor only accets a POST request.")
	}
}

func httpGetTutor(w http.ResponseWriter, r *http.Request) {
	enableCors(w)

	if r.Method == http.MethodGet {
		query := r.URL.Query()
		courseName := helpers.CapString(query.Get("courseName"), 8)
		professorFname := helpers.CapString(query.Get("professorFname"), 50)
		professorLname := helpers.CapString(query.Get("professorLname"), 50)

		c, err := repo.GetCourseByName(courseName)
		if err != nil {
			http.Error(w, "no course named "+courseName+" was found in the system", http.StatusInternalServerError)
			return
		}
		p, err := repo.GetProfessorByName(professorFname, professorLname)
		if err != nil {
			http.Error(w, "no professor named "+professorFname+" "+professorLname+" was found in the system", http.StatusInternalServerError)
			return
		}

		tutors, err := repo.FindTutorsForCourse(*c, *p)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		tJ, err := json.Marshal(tutors)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(tJ)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "course only accets a GET request.")
	}
}
