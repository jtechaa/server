package helpers

func CapString(initial string, maxSize int) string {
	tmp := make([]byte, maxSize)
	newLen := copy(tmp, []byte(initial))

	return string(tmp)[:newLen]
}
