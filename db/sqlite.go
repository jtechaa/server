package db

import (
	"database/sql"
	"errors"
)

// SQLiteRepository implements the Repository interface for SQLite databases.
type SQLiteRepository struct {
	db *sql.DB
}

// NewSQLiteRepo creates a new instance of SQLiteRepository and returns a pointer to it.
func NewSQLiteRepo(db *sql.DB) *SQLiteRepository {
	return &SQLiteRepository{db: db}
}

// Migrate initializes the SQLite database by creating all its tables if they do not exist.
func (r *SQLiteRepository) Migrate() error {
	query := `
    CREATE TABLE IF NOT EXISTS semesters(
		semester_id INTEGER PRIMARY KEY AUTOINCREMENT,
		season VARCHAR(6) NOT NULL,
		year INTEGER NOT NULL
	);

	CREATE TABLE IF NOT EXISTS courses(
		course_id INTEGER PRIMARY KEY AUTOINCREMENT,
		prefix VARCHAR(3) NOT NULL,
		num INTEGER NOT NULL,
		postfix varchar(1)
	);

	CREATE TABLE IF NOT EXISTS professors(
		professor_id INTEGER PRIMARY KEY AUTOINCREMENT,
		fname VARCHAR(50) NOT NULL,
		lname VARCHAR(50) NOT NULL
	);

	CREATE TABLE IF NOT EXISTS courses_taught_by(
		prof_id INTEGER REFERENCES professors (professor_id),
		course_id INTEGER REFERENCES courses (course_id)
	);

	CREATE TABLE IF NOT EXISTS ratings(
		prof_id INTEGER REFERENCES professors (professor_id),
		course_id INTEGER REFERENCES courses (course_id),
		rating TEXT,
		is_secular BOOL
	);

	CREATE TABLE IF NOT EXISTS tutors(
		tutor_id INTEGER PRIMARY KEY AUTOINCREMENT,
		fname VARCHAR(50) NOT NULL,
		lname VARCHAR(50) NOT NULL,
		affiliation VARCHAR(10) NOT NULL
	);

	CREATE TABLE IF NOT EXISTS tutor_course_completion(
		tutor_id REFERENCES tutors (tutor_id),
		course_id REFERENCES courses (course_id)
	);
    `

	_, err := r.db.Exec(query)
	return err
}

func (r *SQLiteRepository) InsertSemester(s Semester) (*Semester, error) {
	res, err := r.db.Exec("INSERT INTO semesters(season, year) values(?,?)", s.Season, s.Year)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	s.ID = id
	return &s, nil
}

// InsertCourse inserts a Course instance into the repository and populates its ID field.
func (r *SQLiteRepository) InsertCourse(c Course) (*Course, error) {
	res, err := r.db.Exec("INSERT INTO courses(prefix, num, postfix) values(?,?,?)", c.Prefix, c.Num, c.Postfix)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	c.ID = id
	return &c, nil
}

// InsertProfessor inserts a Professor instance into the repository and populates its ID field, assuming the Professor instance has only one Course.
// The Professor's ratings are inserted into the ratings table under the Professor's ID and the ID of the single Course in the Professor instance.
func (r *SQLiteRepository) InsertProfessor(p Professor) (*Professor, error) {
	res, err := r.db.Exec("INSERT INTO professors(fname, lname) values(?,?)", p.Fname, p.Lname)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	p.ID = id

	_, err = r.db.Exec("INSERT INTO courses_taught_by(prof_id, course_id) values(?,?)", p.ID, p.Courses[0].ID)
	if err != nil {
		return nil, err
	}

	var c *Course
	for courseName, ratings := range p.SecularRatings {
		c, err = r.GetCourseByName(courseName)
		if err != nil {
			return nil, err
		}

		for _, rating := range ratings {
			_, err = r.db.Exec("INSERT INTO ratings(prof_id, course_id, rating, is_secular) values(?,?,?,?)", p.ID, (*c).ID, rating, true)
			if err != nil {
				return nil, err
			}
		}
	}

	for courseName, ratings := range p.JewishRatings {
		c, err = r.GetCourseByName(courseName)
		if err != nil {
			return nil, err
		}

		for _, rating := range ratings {
			_, err = r.db.Exec("INSERT INTO ratings(prof_id, course_id, rating, is_secular) values(?,?,?,?)", p.ID, (*c).ID, rating, false)
			if err != nil {
				return nil, err
			}
		}
	}

	return &p, nil
}

// GetCourseByName retrieves a Course instance from the database by its name, without any partial matches.
func (r *SQLiteRepository) GetCourseByName(name string) (*Course, error) {
	if len(name) < 7 {
		return nil, errors.New("course name too short")
	}

	// TODO: Course names w/o postfix currently unacceptable
	rows, err := r.db.Query("SELECT * FROM courses WHERE prefix = ? AND num = ? AND postfix = ?", name[:3], name[3:7], name[7:8])
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	if rows.Next() {
		var c Course
		if err := rows.Scan(&c.ID, &c.Prefix, &c.Num, &c.Postfix); err != nil {
			return nil, err
		}
		return &c, nil
	}

	return nil, errors.New("no results for query")
}

// GetProfessorByName retrieves a Professor instance from the database by first and last name, without any partial matches.
func (r *SQLiteRepository) GetProfessorByName(fname string, lname string) (*Professor, error) {
	if len(fname) > 50 || len(lname) > 50 {
		return nil, errors.New("either first or last name too long")
	}

	row := r.db.QueryRow("SELECT * FROM professors WHERE fname = ? and lname = ?", fname, lname)

	var p Professor
	if err := row.Scan(&p.ID, &p.Fname, &p.Lname); err != nil {
		return nil, err
	}

	r.PopulateProfessorFields(&p)
	return &p, nil
}

// PopulateProfessorFields populats the ratings and courses fields of the Professor instance from the database. Assumes ID field is already filled.
func (r *SQLiteRepository) PopulateProfessorFields(prof *Professor) error {
	rows, err := r.db.Query("SELECT course_id FROM courses_taught_by WHERE prof_id = ?", prof.ID)
	if err != nil {
		return err
	}
	defer rows.Close()

	var courses []Course
	for rows.Next() {
		var c Course
		if err := rows.Scan(&c.ID); err != nil {
			return err
		}

		cRow := r.db.QueryRow("SELECT * FROM courses WHERE course_id = ?", c.ID)
		if err := cRow.Scan(&c.ID, &c.Prefix, &c.Num, &c.Postfix); err != nil {
			return err
		}

		courses = append(courses, c)
	}

	(*prof).Courses = courses

	rows, err = r.db.Query("SELECT course_id, rating, is_secular FROM ratings WHERE prof_id = ?", prof.ID)
	if err != nil {
		return err
	}
	defer rows.Close()

	jewish := map[string][]string{}
	secular := map[string][]string{}
	for rows.Next() {
		var courseID int64
		var rating string
		var isSecular bool
		if err := rows.Scan(&courseID, &rating, &isSecular); err != nil {
			return err
		}

		var courseFullName string
		for _, c := range courses {
			if c.ID == courseID {
				courseFullName = c.FullName()
			}
		}
		// TODO: Not considering possibility that c.ID will never equal courseID

		if isSecular {
			secular[courseFullName] = append(secular[courseFullName], rating)
		} else {
			jewish[courseFullName] = append(jewish[courseFullName], rating)
		}
	}

	(*prof).JewishRatings = jewish
	(*prof).SecularRatings = secular

	return nil
}

func (r *SQLiteRepository) InsertTutor(t Tutor) (*Tutor, error) {
	res, err := r.db.Exec("INSERT INTO tutors(fname, lname, affiliation) values(?,?,?)", t.Fname, t.Lname, t.Affiliation)
	if err != nil {
		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	t.ID = id

	for _, course := range t.Courses {
		_, err := r.db.Exec("INSERT INTO tutor_course_completion(tutor_id, course_id) values(?,?)", t.ID, course.ID)
		if err != nil {
			return nil, err
		}
	}

	return &t, nil
}

func (r *SQLiteRepository) FindTutorsForCourse(c Course, p Professor) ([]*Tutor, error) {
	// TODO: we don't actually use the professor parameter.
	rows, err := r.db.Query("SELECT tutor_id FROM tutor_course_completion WHERE course_id = ?", c.ID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	possibleTutors := []*Tutor{}
	for rows.Next() {
		var tutorID int64
		var tutor Tutor
		if err := rows.Scan(&tutorID); err != nil {
			return nil, err
		}

		tRow := r.db.QueryRow("SELECT * FROM tutors WHERE tutor_id = ?", tutorID)
		if err := tRow.Scan(&tutor.ID, &tutor.Fname, &tutor.Lname, &tutor.Affiliation); err != nil {
			return nil, err
		}

		tutor.Courses = []Course{}
		tcRows, err := r.db.Query("SELECT courses.course_id, prefix, num, postfix FROM courses JOIN tutor_course_completion ON courses.course_id = tutor_course_completion.course_id WHERE tutor_id = ?", tutor.ID)
		if err != nil {
			return nil, err
		}

		for tcRows.Next() {
			var c Course
			if err = tcRows.Scan(&c.ID, &c.Prefix, &c.Num, &c.Postfix); err != nil {
				return nil, err
			}
			tutor.Courses = append(tutor.Courses, c)
		}

		possibleTutors = append(possibleTutors, &tutor)
	}

	return possibleTutors, nil
}
