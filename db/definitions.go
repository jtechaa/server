// Package db provides type definitions the JTech academic affairs
// program.
package db

import (
	"reflect"
	"strconv"

	"gitlab.com/jtechaa/server/helpers"
)

// DBType encompasses all types which can be stored in the database.
type DBType interface {
	GetID() int64
}

// Semester stores the Season (Fall, Spring, Summer, etc) and Year that a
// course is taking place within.
type Semester struct {
	ID     int64
	Season string
	Year   int
}

// Course stores the identifier (PrefixNumPostfix, i.e. PHY2048C) and prerequisites for a course.
type Course struct {
	ID      int64
	Prefix  string
	Num     int
	Postfix string
	Prereqs []Course
}

// Professor stores the name, courses, and ratings of a teaching professor.
type Professor struct {
	ID             int64
	Fname          string
	Lname          string
	Courses        []Course
	SecularRatings map[string][]string // full course name is the key
	JewishRatings  map[string][]string
}

type Tutor struct {
	ID          int64
	Fname       string
	Lname       string
	Courses     []Course
	Affiliation string
}

// Repository exposes key functions across multiple kinds of databases. Separate implementations for each database provider (SQLite, Postgres) are required.
type Repository interface {
	Migrate() error
	InsertSemester(s Semester) (*Semester, error)
	InsertCourse(c Course) (*Course, error)
	InsertProfessor(p Professor) (*Professor, error)

	AllSemesters() ([]Semester, error)
	AllCourses() ([]Course, error)
	AllProfessors() ([]Professor, error)

	GetCourseByName(name string) (*Course, error)

	Update(id int64, t reflect.Type, updated DBType) error
	Delete(id int64, t reflect.Type) error
}

func InitSemester(season string, year string) (Semester, error) {
	const MAX_CHARS_SEASON = 6

	yr, err := strconv.Atoi(year)
	if err != nil {
		return Semester{}, err
	}

	return Semester{Season: season[:MAX_CHARS_SEASON+1], Year: yr}, nil
}

func InitCourse(prefix string, num string, postfix string) (Course, error) {
	const MAX_CHARS_PREFIX = 3
	const MAX_CHARS_NUM = 4
	const MAX_CHARS_POSTFIX = 1

	n, err := strconv.Atoi(num[:MAX_CHARS_NUM])
	if err != nil {
		return Course{}, err
	}

	var cappedPostfix string
	if len(postfix) == 0 {
		cappedPostfix = ""
	} else {
		cappedPostfix = postfix[:MAX_CHARS_POSTFIX]
	}

	return Course{Prefix: prefix[:MAX_CHARS_PREFIX], Num: n, Postfix: cappedPostfix, Prereqs: []Course{}}, nil
}

func InitProfessor(fname string, lname string) Professor {
	return Professor{Fname: helpers.CapString(fname, 50), Lname: helpers.CapString(lname, 50), Courses: []Course{}, SecularRatings: map[string][]string{}, JewishRatings: map[string][]string{}}
}

func InitTutor(fname string, lname string, affiliation string) Tutor {
	return Tutor{Fname: helpers.CapString(fname, 50), Lname: helpers.CapString(lname, 50), Courses: []Course{}, Affiliation: helpers.CapString(affiliation, 10)}
}

func (s Semester) GetID() int64 {
	return s.ID
}

func (c Course) GetID() int64 {
	return c.ID
}

func (c Course) FullName() string {
	return c.Prefix + strconv.Itoa(c.Num) + c.Postfix
}

func (p Professor) GetID() int64 {
	return p.ID
}

func (t Tutor) GetID() int64 {
	return t.ID
}
